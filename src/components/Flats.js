import React, { useEffect, useState } from "react";
import data from "../data.json";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHeart as emptyHeart } from "@fortawesome/free-regular-svg-icons";
import { faHeart as filledHeart } from "@fortawesome/free-solid-svg-icons";

function Flats() {
  const [like, setLike] = useState({});

  const setFav = (id) => {
    const updateLike = {}

    if (like.hasOwnProperty(id) && like[id] === filledHeart) {
      updateLike[id] =  emptyHeart;
    } else {
      updateLike[id] = filledHeart;
    }

    setLike({
      ...like,
      ...updateLike
    });

    console.log(like);
    console.log(updateLike);
  };

  const flatAddress = (
    id,
    city,
    street,
    house,
    flat,
    area,
    unit,
    title,
    type,
    fname,
    mname,
    lname
  ) => (
    <div className="flats-inner">
      <div>
        <h3>
          Адрес: {city} , {street} {house},<br /> кв {flat}
        </h3>
        <h5>
          Описание: {area} {unit} {title}
        </h5>
        <h5>Автор объявления: {type}</h5>
        <h5>
          {fname} {mname} {lname}
        </h5>
        <br />
      </div>
      <FontAwesomeIcon onClick={()=>setFav(id)} icon={like.hasOwnProperty(id) ? like[id] : emptyHeart}/>
    </div>
  );

  const allFlats = data.response?.map((item) =>
    flatAddress(
      item.id,
      item.attributes.address.city,
      item.attributes.address.street,
      item.attributes.address.house,
      item.attributes.address.room,
      item.attributes.area,
      item.attributes.unit,
      item.attributes.title,
      item.relationships.type,
      item.relationships.attributes.first_name,
      item.relationships.attributes.middle_name,
      item.relationships.attributes.last_name
    )
  );

  return <div className="flats">{allFlats}</div>;
}

export default Flats;
